package inf319;

import java.util.HashMap;
import java.util.Map;

public class GerenteProgressao {
	private Progressao progressao;
	private int termoCorrente;
	private Map<Integer, Integer> cache;
	
	public GerenteProgressao(Progressao progressao) {
		this.progressao = progressao;
		termoCorrente = 0;
		cache = new HashMap<Integer, Integer>();
		cache.put(termoCorrente, (int) progressao.inicia());
	}
	
	public int inicia() {
		termoCorrente = 0;
		return this.cache.get(0);
	}
	
	public int proxTermo() {
		termoCorrente++;
		if (cache.containsKey(termoCorrente)) {
			return cache.get(termoCorrente);
		}
		int value = (int) progressao.proxTermo();
		cache.put(termoCorrente, value);
		return value;
	}
	
	public int iesimoTermo(int i) {
		if (cache.containsKey(termoCorrente)) {
			return cache.get(termoCorrente);
		}
		int iesimo = proxTermo();
		for (int j = termoCorrente; j < i; j++) {
			iesimo = proxTermo();
		}
		return iesimo;
	}
}
