package inf319;

public class ProgressaoAritmetica extends Progressao {

	private long incremento;
	private long valorInicial;

	public ProgressaoAritmetica() {
		this(0);
	}

	public ProgressaoAritmetica(int valorInicial) {
		inicia(valorInicial, 1);
	}

	public ProgressaoAritmetica(int valorInicial, int incremento) {
		inicia(valorInicial, incremento);
	}

	public long inicia() {
		this.valCor = this.valorInicial;
		return valCor;
	}
	
	public long inicia(int valorInicial) {
		this.valCor = valorInicial;
		this.valorInicial = valorInicial;
		return valCor;
	}

	public long inicia(int valorInicial, int incremento) {
		this.incremento = incremento;
		return inicia(valorInicial);
	}

	public long proxTermo() {
		valCor += incremento;
		return valCor;
	}

}
