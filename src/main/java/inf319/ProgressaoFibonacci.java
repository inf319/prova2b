package inf319;

public class ProgressaoFibonacci extends Progressao {

	private long valPrev;
	private long valorInicial;
	private long valorPrevInicial;

	public ProgressaoFibonacci() {
		this(0);
	}

	public ProgressaoFibonacci(int valorInicial) {
		inicia(valorInicial, 1);
	}

	public ProgressaoFibonacci(int valorInicial, int valorPrevio) {
		inicia(valorInicial, valorPrevio);
	}

	public long inicia() {
		this.valCor = this.valorInicial;
		this.valPrev = this.valorPrevInicial;
		return valCor;
	}

	public long inicia(int valorInicial) {
		this.valCor = valorInicial;
		this.valorInicial = valorInicial;
		return valCor;
	}

	public long inicia(int valorInicial, int valorPrevio) {
		this.valPrev = valorPrevio;
		this.valorPrevInicial = valorPrevio;
		return inicia(valorInicial);
	}

	public long proxTermo() {
		valCor += valPrev;
		valPrev = valCor - valPrev;
		return valCor;
	}
}
