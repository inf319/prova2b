package inf319;

public class ProgressaoGeometrica extends Progressao {

	private long base;
	private long valorInicial;

	public ProgressaoGeometrica() {
		this(1);
	}

	public ProgressaoGeometrica(int valorInicial) {
		inicia(valorInicial, 2);
	}

	public ProgressaoGeometrica(int valorInicial, int base) {
		inicia(valorInicial, base);
	}

	public long inicia() {
		this.valCor = this.valorInicial;
		return valCor;
	}

	public long inicia(int valorInicial) {
		this.valCor = valorInicial;
		this.valorInicial = valorInicial;
		return valCor;
	}

	public long inicia(int valorInicial, int base) {
		this.base = base;
		return inicia(valorInicial);
	}

	public long proxTermo() {
		valCor *= base;
		return valCor;
	}

}
